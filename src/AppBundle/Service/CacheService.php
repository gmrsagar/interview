<?php

namespace AppBundle\Service;

use Predis;

/**
* Here you have to implement a CacheService with the operations above.
* It should contain a failover, which means that if you cannot retrieve
* data you have to hit the Database.
**/
class CacheService
{

    protected $client;

    public function __construct($host, $port, $prefix)
    {
        // Redis configuration
        $vm = array(
            'host'     => $host,
            'port'     => $port,
            'timeout' => 0.05 // (expressed in seconds) used to connect to a Redis server after which an exception is thrown.
        );

        $this->client = new Predis\Client($vm);
        
    }

    public function get($key)
    {
        $value = $this->client -> get($key);
        return $value;

    }

    public function set($key, $value)
    {
        $set = $this->client->set($key, $value);
        $this->client->expire($key, 86400); //Set one day expiration on cache key
        return $set;
    }

    public function del($key)
    {
        $this->client->del($key);
    }

}
