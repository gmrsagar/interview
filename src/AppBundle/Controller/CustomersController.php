<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Service\CacheService;

class CustomersController extends Controller
{
    private $port;

    public function __construct()
    {
        /**
         * 
         * Check redis port availability
         * 
         */
        $this->port = $this->stest('127.0.0.1', '6379');
    }

    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {   

        if ($this->port){

            $cacheService = $this->get('cache_service');

            $customers = $cacheService->get('customers');

            // TODO: Implement logic here
            if (empty($customers)) {
                $database = $this->get('database_service')->getDatabase();
                $customers = $database->customers->find();  
                $customers = json_encode(iterator_to_array($customers));
                $customers = $cacheService->set('customers', $customers);

                if($customers) {
                    $customers = json_decode($cacheService->get('customers'));
                    return new JsonResponse($customers);
                }
            }

            $cacheCustomers = json_decode($customers);
            return new JsonResponse($cacheCustomers);
        }
        else {
            $database = $this->get('database_service')->getDatabase();
            $customers = $database->customers->find();
            $customers = iterator_to_array($customers);
            return new JsonResponse($customers);
        }

    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $database = $this->get('database_service')->getDatabase();
        $customers = json_decode($request->getContent());

        if (empty($customers)) {
            return new JsonResponse(['status' => 'No donuts for you'], 400);
        }

        foreach ($customers as $customer) {
            $database->customers->insertOne($customer);
        }

        if ($this->port) {
            $cacheService = $this->get('cache_service')->del('customers');
        }

        return new JsonResponse(['status' => 'Customers successfully created']);
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
        $database = $this->get('database_service')->getDatabase();
        $database->customers->drop();

        if ($this->port) {
            $cacheService = $this->get('cache_service')->del('customers');
        }

        return new JsonResponse(['status' => 'Customers successfully deleted']);
    }

    /** 
     * Chech redis port
    */
    public function stest($ip, $portt) {
        $fp = @fsockopen($ip, $portt, $errno, $errstr, 0.1);
        if (!$fp) {
            return false;
        } else {
            fclose($fp);
            return true;
        }
    }
}
